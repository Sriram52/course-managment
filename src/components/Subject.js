import React from 'react';
import { TextField, MenuItem, makeStyles } from '@material-ui/core'

export default function Subject(props) {
    const useStyles = makeStyles((theme) => ({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
    }));
    const classes = useStyles();
    return (
        <form className={classes.root}>
            <TextField
                id="outlined-select-subject"
                select
                label="Select the subject"
                value={props.subject}
                onChange={props.handleChange('subject')}
                helperText={!props.subject ? "Please select your subject" : null}
                variant="outlined"
            >
                {subjects.map((option) => (
                    <MenuItem key={option.label} value={option.label}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
        </form>
    )
}

const subjects = [
    { label: 'English' },
    { label: 'Telugu' },
    { label: 'Hindi' },
    { label: 'Social' },
    { label: 'Science' },
    { label: 'Mathematics' }
]
