import React from 'react';
import { TextField, MenuItem, makeStyles } from '@material-ui/core'

export default function Class(props) {
    const useStyles = makeStyles((theme) => ({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
    }));
    const cl = useStyles();
    return (
        <form className={cl.root}>
            <TextField
                id="outlined-select-class"
                select
                label="Select the Class"
                value={props.cla}
                onChange={props.handleChange('cla')}
                helperText={!props.cla ? "Please select your class" : null}
                variant="outlined"
            >
                {classes.map((option) => (
                    <MenuItem key={option.title} value={option.title}>
                        {option.title}
                    </MenuItem>
                ))}
            </TextField>
        </form>
    )
}

const classes = [
    {title: '10'} , 
    {title: '9'},
    {title: '8'},
    {title: '7'},
    {title: '6'},
    {title: '5'},
    {title: '4'},
    {title: '3'},
    {title: '2'},
    {title: '1'},
    {title: 'UKG'},
    {title: 'LKG'}
]