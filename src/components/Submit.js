import React, { Component } from 'react';
import { Button } from '@material-ui/core';

class Submit extends Component {
    state = {}
    render() {
        return (
            <Button variant="contained" color="primary" onClick={()=>console.log(this.props)}>
                Submit
            </Button>
        );
    }
}

export default Submit;