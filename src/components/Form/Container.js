import React, { Component } from 'react';
import Presentation from './Presentation';


let name = new Array()
let link = new Array()

class Container extends Component {

    state = {
        cla: "",
        section: "",
        subject: "",
        topicLink: "",
        topicName: "",
    }

    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    }

    render() {
        return (
            <Presentation
                {...this.state}
                name={name}
                link={link}
                handleChange={this.handleChange}
            />
        )
    }
}

export default Container;






