import React from 'react';
import { Paper, makeStyles, AppBar, Toolbar, Typography } from '@material-ui/core'
import Class from '../Class';
import Subject from '../Subject';
import Section from '../Section';
import AddTopic from '../Topics/AddTopic'
import Submit from '../Submit';

export default function Presentation(props) {
    const classes = useStyles();
    const { n, cla, section, subject, topicLink, topicName, handleChange, name, link } = props
    return (
        <form>
            <AppBar position="static">
                <Toolbar variant="dense">
                    <Typography variant="h6" color="inherit">
                        Course
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className={classes.paper}>
                <Class
                    cla={cla}
                    handleChange={handleChange}
                />
                <Section
                    section={section}
                    handleChange={handleChange}
                />
                <Subject
                    subject={subject}
                    handleChange={handleChange}
                />
                <AddTopic
                    topicLink={topicLink}
                    topicName={topicName}
                    name={name}
                    link={link}
                    handleChange={handleChange}
                />
                <Submit
                    {...props}
                />
            </div>
        </form>
    )
}

const useStyles = makeStyles((theme) => ({
    paper: {
        alignItems: 'center',
        padding: 20
    },
}))
