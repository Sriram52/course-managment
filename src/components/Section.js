import React from 'react';
import { TextField, MenuItem, makeStyles } from '@material-ui/core'

export default function Section(props) {
    const useStyles = makeStyles((theme) => ({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
    }));
    const classes = useStyles();
    return (
        <form className={classes.root}>
            <TextField
                id="outlined-select-section"
                select
                label="Select the section"
                value={props.section}
                onChange={props.handleChange('section')}
                helperText={!props.section ? "Please select your section" : null}
                variant="outlined"
            >
                {sections.map((option) => (
                    <MenuItem key={option.title} value={option.title}>
                        {option.title}
                    </MenuItem>
                ))}
            </TextField>
        </form>
    )
}

const sections = [
    {title: 'IIT'} , 
    {title: 'A'},
    {title: 'B'},
    {title: 'C'},
    {title: 'D'}
]
