import React, { Component } from 'react';
import { Button, TextField, CssBaseline, Grid } from '@material-ui/core';
import TopicLink from './TopicLink';
import TopicName from './TopicName';


class AddTopic extends Component {
    state = {
        topicClicked: 0
    }


    handleTopicClick = () => {
        this.setState({ topicClicked: this.state.topicClicked + 1 });
    }


    storeTopicData = () => {
        this.props.name.push(this.props.topicName)
        this.props.link.push(this.props.topicLink)
        console.log(this.props.name)
        console.log(this.props.link)
    }

    render() {
        const { topicLink, topicName, handleChange } = this.props

        let topic = new Array()
        for (let index = 0; index < this.state.topicClicked; index++) {
            topic.push('item: ' + index)
        }

        return (
            <React.Fragment>
                <Button variant="contained" color="primary" onClick={this.handleTopicClick}>
                    Add Topic
                </Button>
                {
                    topic.map(item =>
                        this.state.topicClicked ?
                            <Grid container spacing={3}>
                                <Grid item style={{ marginTop: 20 }}>
                                    <TopicName
                                        id={"topic"}
                                        topicName={topicName}
                                        handleChange={handleChange}
                                    />
                                </Grid>
                                <Grid item style={{ margin: 20 }}>
                                    <TopicLink
                                        id={"topic"}
                                        topicLink={topicLink}
                                        handleChange={handleChange}
                                    />
                                </Grid>
                                <Grid item style={{ margin: 20 }}>
                                    <Button variant="contained" color="primary" onClick={this.storeTopicData}>
                                        Done
                                    </Button>
                                </Grid>
                            </Grid>
                            : null
                    )
                }
            </React.Fragment>
        );
    }
}

export default AddTopic;