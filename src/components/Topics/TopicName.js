import React, { Component } from 'react';
import { TextField, Grid } from '@material-ui/core';


class TopicName extends Component {
    render() { 
        return (
            <TextField
                id="topicName" 
                label={"Enter the "+this.props.id+" Name"}
                variant="outlined"
                onChange={this.props.handleChange('topicName')}
            />
        );
    }
}
 
export default TopicName;