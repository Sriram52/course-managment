import React, { Component } from 'react';
import { TextField } from '@material-ui/core';

class TopicLink extends Component {
    render() { 
        return (
            <TextField
                id="topicLink" 
                label={"Paste the "+this.props.id+" link"}
                variant="outlined"
                onChange={this.props.handleChange('topicLink')}
            />
        );
    }
}
 
export default TopicLink;